/**
 * 
 */
package com.learning;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * @author durgeshsingh
 *
 */
public class FluxAndMonoTimeTest {
	
	@Test
	public void infiniteSequence()	{
		Flux<Long> infiniteFlux =Flux.interval(Duration.ofMillis(100)).log(); // starts from  0 ---> ........
		infiniteFlux.subscribe(element -> { System.out.println(element); } );
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void infiniteSequenceTest()	{
		Flux<Long> infiniteFlux =Flux.interval(Duration.ofMillis(100)).log() // starts from  0 ---> ........
				.take(3)
				.log();
		StepVerifier.create(infiniteFlux)
		.expectSubscription()
		.expectNext(0L,1L,2L)
		.verifyComplete();
	}
	
	@Test
	public void infiniteSequenceMap()	{
		Flux<Integer> infiniteFlux =Flux.interval(Duration.ofMillis(100)) // starts from  0 ---> ........
				.delayElements(Duration.ofSeconds(1))
				.map(l -> Integer.valueOf(l.intValue()))
				.take(3)
				.log();
		StepVerifier.create(infiniteFlux)
		.expectSubscription()
		.expectNext(0,1,2)
		.verifyComplete();
	}

}
