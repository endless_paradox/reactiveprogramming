/**
 * 
 */
package com.learning;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import com.learning.exception.CustomException;

import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import reactor.util.retry.Retry;

/**
 * @author durgeshsingh
 *
 */
public class FluxAndMonoErrorTest {
	
	@Test
	public void fluxErrorHandling()	{
		
		Flux<String> stringFlux = Flux.just("A","B","C")
				.concatWith(Flux.error(new RuntimeException("Exception Occured")))
				.concatWith(Flux.just("D"))
				.onErrorResume(e -> {
					System.out.println("Exception is: " + e);
					return Flux.just("default", "default1");
				});
		
		StepVerifier.create(stringFlux.log())
		.expectSubscription()
		.expectNext("A","B","C")
//		.expectError(RuntimeException.class)
//		.verify();
		.expectNext("default", "default1")
		.verifyComplete();
		
	}
	
	@Test
	public void fluxErrorHandling_OnErrorRetrun()	{
		
		Flux<String> stringFlux = Flux.just("A","B","C")
				.concatWith(Flux.error(new RuntimeException("Exception Occured")))
				.concatWith(Flux.just("D"))
				.onErrorReturn("default");
		
		StepVerifier.create(stringFlux.log())
		.expectSubscription()
		.expectNext("A","B","C")
		.expectNext("default")
		.verifyComplete();
		
	}
	
	@Test
	public void fluxErrorHandling_OnErrorMap()	{
		
		Flux<String> stringFlux = Flux.just("A","B","C")
				.concatWith(Flux.error(new RuntimeException("Exception Occured")))
				.concatWith(Flux.just("D"))
				.onErrorMap(e -> new CustomException(e));
		
		StepVerifier.create(stringFlux.log())
		.expectSubscription()
		.expectNext("A","B","C")
//		.expectNext("default")
//		.verifyComplete();
		.expectError(CustomException.class)
		.verify();
		
	}
	
	@Test
	public void fluxErrorHandling_withRetry()	{
		
		Flux<String> stringFlux = Flux.just("A","B","C")
				.concatWith(Flux.error(new RuntimeException("Exception Occured")))
				.concatWith(Flux.just("D"))
				.onErrorMap(e -> new CustomException(e))
				.retry(2);
		
		StepVerifier.create(stringFlux.log())
		.expectSubscription()
		.expectNext("A","B","C")
		.expectNext("A","B","C")
		.expectNext("A","B","C")
//		.expectNext("default")
//		.verifyComplete();
		.expectError(CustomException.class)
		.verify();
		
	}
	
	@Test
	public void fluxErrorHandling_withRetryBackOff()	{
		
		Flux<String> stringFlux = Flux.just("A","B","C")
				.concatWith(Flux.error(new RuntimeException("Exception Occured")))
				.concatWith(Flux.just("D"))
				.onErrorMap(e -> new CustomException(e))
				.retryWhen(Retry.backoff(2, Duration.ofSeconds(2)).onRetryExhaustedThrow((e, t) -> {
					throw new RuntimeException("Exception Occured");
				}));
		
		StepVerifier.create(stringFlux.log())
		.expectSubscription()
		.expectNext("A","B","C")
		.expectNext("A","B","C")
		.expectNext("A","B","C")
//		.expectNext("default")
//		.verifyComplete();
		.expectError(RuntimeException.class)
		.verify();
		
	}

}
