/**
 * 
 */
package com.learning;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

/**
 * @author durgeshsingh
 *
 */
public class ColdAndHotPublisherTest {
	
	@Test
	public void coldPublisherTest() throws InterruptedException	{
		
		Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
				.delayElements(Duration.ofSeconds(1));
		
		stringFlux.subscribe(s -> System.out.println("Subscriber 1: " + s));
		
		Thread.sleep(2000);
		
		
		stringFlux.subscribe(s -> System.out.println("Subscriber 2: " + s));
		
		Thread.sleep(4000);
		
		
	}
	
	@Test
	public void hotPublisherTest() throws InterruptedException	{
	
		Flux<String> stringFlux = Flux.just("A","B","C","D","E","F")
				.delayElements(Duration.ofSeconds(1));
		
		ConnectableFlux<String> stringConnectableFlux = stringFlux.publish();
		stringConnectableFlux.connect();
		
		stringConnectableFlux.subscribe(s -> System.out.println("Subscriber 1: " + s));
		
		Thread.sleep(3000);
		
		
		stringConnectableFlux.subscribe(s -> System.out.println("Subscriber 2: " + s));
		
		Thread.sleep(4000);
	
	}

}
