/**
 * 
 */
package com.learning;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * @author durgeshsingh
 *
 */
public class FluxAndMonoBackPrerssure {
	
	@Test
	public void backPressureTest()	{
		
		Flux<Integer> finiteFlux = Flux.range(1, 10);

		StepVerifier.create(finiteFlux.log())
		.expectSubscription()
		.thenRequest(1)
		.expectNext(1)
		.thenRequest(1)
		.expectNext(2)
		.thenCancel()
		.verify();
		
	}
	
	@Test
	public void backPressure()	{
		
		Flux<Integer> finiteFlux = Flux.range(1, 10);
		
		finiteFlux.log().doOnNext(System.out::println).subscribeWith(new BaseSubscriber<Integer>() {
			@Override
			protected void hookOnSubscribe(Subscription subscription) {
				requestUnbounded();
			}
			
			@Override
			protected void hookOnComplete() {
				System.out.println("Done");
			}
		});
		
	}
	
	@Test
	public void customizedBackPressure()	{
		
		Flux<Integer> finiteFlux = Flux.range(1, 10);
		
		finiteFlux.log().subscribe(new BaseSubscriber<Integer>() {
			@Override
			protected void hookOnSubscribe(Subscription subscription) {
				request(2);
			}
			
			@Override
			protected void hookOnNext(Integer value) {
				System.out.println(value);
				
			}
			
			@Override
			protected void hookOnComplete() {
				System.out.println("Done");
			}
		});
		
	}

}
