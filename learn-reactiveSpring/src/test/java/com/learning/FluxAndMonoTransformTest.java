/**
 * 
 */
package com.learning;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

/**
 * @author durgeshsingh
 *
 */
@SpringBootTest
public class FluxAndMonoTransformTest {

	@Test
	public void transformUsingFlatMap() {

		Flux<String> stringFlux = Flux.fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F")).flatMap(s -> {
			return Flux.fromIterable(convertToList(s));
		}).log(); // db or external calls that returns a flux -> s -> Flux<String>

		StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();

	}

	/**
	 * @param s
	 * @return
	 */
	private List<String> convertToList(String s) {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Arrays.asList(s, "newValue");
	}

	@Test
	public void transformUsingFlatMap_usingparallel() {

		Flux<String> stringFlux = Flux.fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F"))
				.window(2) //Flux<Flux<String>> -> (A,B), (C,D), (E,F)
				.flatMap((s) -> 
					s.map(this::convertToList).subscribeOn(Schedulers.parallel()))
				.flatMap(s -> Flux.fromIterable(s)) //Flux<String>
				.log(); // db or external calls that returns a flux -> s -> Flux<String>

		StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();

	}
	
	@Test
	public void transformUsingFlatMap_parallel_maintain_order() {

		Flux<String> stringFlux = Flux.fromIterable(Arrays.asList("A", "B", "C", "D", "E", "F"))
				.window(2) //Flux<Flux<String>> -> (A,B), (C,D), (E,F)
//				.concatMap((s) -> 
//					s.map(this::convertToList).subscribeOn(Schedulers.parallel()))
				.flatMapSequential((s) -> 
				s.map(this::convertToList).subscribeOn(Schedulers.parallel()))
				.flatMap(s -> Flux.fromIterable(s)) //Flux<String>
				.log(); // db or external calls that returns a flux -> s -> Flux<String>

		StepVerifier.create(stringFlux).expectNextCount(12).verifyComplete();

	}

}
