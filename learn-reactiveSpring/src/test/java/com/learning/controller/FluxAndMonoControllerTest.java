/**
 * 
 */
package com.learning.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

/**
 * @author durgeshsingh
 *
 */

@WebFluxTest
@AutoConfigureWebTestClient(timeout = "36000")
@ExtendWith(SpringExtension.class)
public class FluxAndMonoControllerTest {
	
	
	@Autowired
	WebTestClient webTestClient;
	
	
	@Test
	public void flux_approach1()	{
		
		Flux<Integer> fluxInteger = webTestClient.get().uri("/flux").accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isOk()
		.returnResult(Integer.class)
		.getResponseBody();
		
		StepVerifier.create(fluxInteger)
		.expectSubscription()
		.thenAwait(Duration.ofSeconds(4))
		.expectNext(1,2,3,4)
		.verifyComplete();
		
	}
	
	@Test
	public void flux_approach2()	{
		
		webTestClient.get().uri("/flux").accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isOk()
		.expectHeader().contentType(MediaType.APPLICATION_JSON)
		.expectBodyList(Integer.class)
		.hasSize(4);
		
	}
	
	@Test
	public void flux_approach3()	{
		
		List<Integer> expectedInteger = Arrays.asList(1,2,3,4);
		
		EntityExchangeResult<List<Integer>> entityExchangeResult = webTestClient.get().uri("/flux").accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isOk()
		.expectBodyList(Integer.class)
		.returnResult();
		
		Assertions.assertThat(entityExchangeResult.getResponseBody()).isEqualTo(expectedInteger);
	}
	
	@Test
	public void flux_approach4()	{
		
		List<Integer> expectedInteger = Arrays.asList(1,2,3,4);
		
		webTestClient.get().uri("/flux").accept(MediaType.APPLICATION_JSON)
		.exchange()
		.expectStatus().isOk()
		.expectBodyList(Integer.class)
		.consumeWith(response -> {
			assertThat(response.getResponseBody()).isEqualTo(expectedInteger);
		});
	}
	
	@Test
	public void fluxStream()	{
		
		Flux<Long> streamfluxLong = webTestClient.get().uri("/fluxstream").accept(MediaType.APPLICATION_NDJSON)
				.exchange()
				.expectStatus().isOk()
				.returnResult(Long.class)
				.getResponseBody();
		
		StepVerifier.create(streamfluxLong)
		.expectSubscription()
		.expectNext(0l)
		.expectNext(1l)
		.expectNext(2l)
		.thenCancel()
		.verify();
		
	}
	
	@Test
	public void monoTest()	{
		
		Integer expectedValue = Integer.valueOf(1);
		
		webTestClient.get().uri("/mono")
			.accept(MediaType.APPLICATION_JSON)
			.exchange()
			.expectStatus().isOk()
			.expectBody(Integer.class)
			.consumeWith(element -> {
				assertThat(element.getResponseBody()).isEqualTo(expectedValue);
			});
		
	}
	
	

}
