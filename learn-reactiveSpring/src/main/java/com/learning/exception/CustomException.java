/**
 * 
 */
package com.learning.exception;

/**
 * @author durgeshsingh
 *
 */
public class CustomException extends Throwable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	/**
	 * 
	 */
	public CustomException(String message) {
		this.message = message;
	}
	
	public CustomException(Throwable e) {
		this.message = e.getMessage();
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
